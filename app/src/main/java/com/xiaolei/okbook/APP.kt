package com.xiaolei.okbook

import android.support.multidex.MultiDexApplication
import com.tencent.bugly.crashreport.CrashReport
import com.xiaolei.okbook.Natives.NativeUtil
import com.xiaolei.okbook.Nets.BaseNetCore


/**
 * Created by xiaolei on 2018/4/20.
 */
class APP : MultiDexApplication()
{
    override fun onCreate()
    {
        registerActivityLifecycleCallbacks(LifeCycle)
        BaseNetCore.initRetrofit(this)
        CrashReport.initCrashReport(this, NativeUtil.getBuglyAppId(), false)
        super.onCreate()
    }
}