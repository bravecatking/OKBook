package com.xiaolei.okbook.Natives

/**
 * Created by xiaolei on 2018/4/20.
 */
object NativeUtil
{
    init
    {
        System.loadLibrary("native-lib")
    }
    
    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    external fun stringFromJNI(): String
    
    external fun getBuglyAppId(): String
}