#include <jni.h>
#include <string>

using namespace std;

extern "C" JNIEXPORT jstring

JNICALL Java_com_xiaolei_okbook_Natives_NativeUtil_stringFromJNI(JNIEnv *env, jobject /* this */) {
    string hello = "欢迎来到OK小说，妈的免费！";
    return env->NewStringUTF(hello.c_str());
}

extern "C" JNIEXPORT jstring
JNICALL Java_com_xiaolei_okbook_Natives_NativeUtil_getBuglyAppId(JNIEnv *env, jobject /* this */) {
    string appId = "d7a2ab5373";
    return env->NewStringUTF(appId.c_str());
}
